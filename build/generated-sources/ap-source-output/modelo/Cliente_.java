package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Factura;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-05T14:46:33")
@StaticMetamodel(Cliente.class)
public class Cliente_ { 

    public static volatile SingularAttribute<Cliente, String> primerNom;
    public static volatile SingularAttribute<Cliente, Integer> idCliente;
    public static volatile SingularAttribute<Cliente, String> segundoApe;
    public static volatile SingularAttribute<Cliente, String> numContacto;
    public static volatile ListAttribute<Cliente, Factura> facturaList;
    public static volatile SingularAttribute<Cliente, String> segundoNom;
    public static volatile SingularAttribute<Cliente, String> primerApe;

}