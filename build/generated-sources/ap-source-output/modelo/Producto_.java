package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.FactMovto;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-05T14:46:33")
@StaticMetamodel(Producto.class)
public class Producto_ { 

    public static volatile SingularAttribute<Producto, String> nombrePro;
    public static volatile ListAttribute<Producto, FactMovto> factMovtoList;
    public static volatile SingularAttribute<Producto, String> descripProducto;
    public static volatile SingularAttribute<Producto, String> valorPro;
    public static volatile SingularAttribute<Producto, Integer> idProducto;

}