package modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import modelo.Factura;
import modelo.Producto;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-05T14:46:33")
@StaticMetamodel(FactMovto.class)
public class FactMovto_ { 

    public static volatile SingularAttribute<FactMovto, Integer> idMovto;
    public static volatile SingularAttribute<FactMovto, Factura> idFactura;
    public static volatile SingularAttribute<FactMovto, Integer> cantidad;
    public static volatile SingularAttribute<FactMovto, Producto> idProducto;

}