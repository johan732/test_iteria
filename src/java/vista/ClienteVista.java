/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import logica.ClienteLogicaLocal;
import modelo.Cliente;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author johan
 */
@ManagedBean
@RequestScoped
public class ClienteVista {

    @EJB
    private ClienteLogicaLocal clienteLogica;

    private List<Cliente> listaClientes;

    private InputText txtPrimerNombre;
    private InputText txtSegundoNombre;
    private InputText txtPrimerApellido;
    private InputText txtSegundoApellido;
    private InputText txtNumContacto;

    private Cliente selectedCliente;

    /**
     * Creates a new instance of ClienteVista
     */
    public ClienteVista() {
    }

    public List<Cliente> getListaClientes() {
        listaClientes = clienteLogica.listAll();
        return listaClientes;
    }

    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public ClienteLogicaLocal getClienteLogica() {
        return clienteLogica;
    }

    public void setClienteLogica(ClienteLogicaLocal clienteLogica) {
        this.clienteLogica = clienteLogica;
    }

    public InputText getTxtPrimerNombre() {
        return txtPrimerNombre;
    }

    public void setTxtPrimerNombre(InputText txtPrimerNombre) {
        this.txtPrimerNombre = txtPrimerNombre;
    }

    public InputText getTxtSegundoNombre() {
        return txtSegundoNombre;
    }

    public void setTxtSegundoNombre(InputText txtSegundoNombre) {
        this.txtSegundoNombre = txtSegundoNombre;
    }

    public InputText getTxtPrimerApellido() {
        return txtPrimerApellido;
    }

    public void setTxtPrimerApellido(InputText txtPrimerApellido) {
        this.txtPrimerApellido = txtPrimerApellido;
    }

    public InputText getTxtSegundoApellido() {
        return txtSegundoApellido;
    }

    public void setTxtSegundoApellido(InputText txtSegundoApellido) {
        this.txtSegundoApellido = txtSegundoApellido;
    }

    public InputText getTxtNumContacto() {
        return txtNumContacto;
    }

    public void setTxtNumContacto(InputText txtNumContacto) {
        this.txtNumContacto = txtNumContacto;
    }

    public Cliente getSelectedCliente() {
        return selectedCliente;
    }

    public void setSelectedCliente(Cliente selectedCliente) {
        this.selectedCliente = selectedCliente;
    }

    //-------------- Funciones ---------------
    public void limpiarCampos() {
        txtPrimerNombre.setValue("");
        txtSegundoNombre.setValue("");
        txtPrimerApellido.setValue("");
        txtSegundoApellido.setValue("");
        txtNumContacto.setValue("");
    }

    public void seleccionarCliente(SelectEvent e) {
        selectedCliente = (Cliente) e.getObject();
        txtPrimerNombre.setValue(selectedCliente.getPrimerNom());
        txtSegundoNombre.setValue(selectedCliente.getSegundoNom());
        txtPrimerApellido.setValue(selectedCliente.getSegundoNom());
        txtSegundoApellido.setValue(selectedCliente.getSegundoNom());
        txtNumContacto.setValue(selectedCliente.getSegundoNom());
    }

    public void nuevoCliente() {
        try {
            String primerNombre = txtPrimerNombre.getValue().toString();
            String segundoNombre = txtSegundoNombre.getValue().toString();
            String primerApellido = txtPrimerApellido.getValue().toString();
            String segundoApellido = txtSegundoApellido.getValue().toString();
            String numContacto = txtNumContacto.getValue().toString();

            Cliente nuevoCliente = new Cliente();
            nuevoCliente.setPrimerNom(primerNombre);
            nuevoCliente.setSegundoNom(segundoNombre);
            nuevoCliente.setPrimerApe(primerApellido);
            nuevoCliente.setSegundoApe(segundoApellido);
            nuevoCliente.setNumContacto(numContacto);

            clienteLogica.store(nuevoCliente);
            limpiarCampos();

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensaje", ex.getMessage()));
        }
    }

    public void actualizarCliente() {
        try {
            String primerNombre = txtPrimerNombre.getValue().toString();
            String segundoNombre = txtSegundoNombre.getValue().toString();
            String primerApellido = txtPrimerApellido.getValue().toString();
            String segundoApellido = txtSegundoApellido.getValue().toString();
            String numContacto = txtNumContacto.getValue().toString();

            Cliente cliente = selectedCliente;
            cliente.setPrimerNom(primerNombre);
            cliente.setSegundoNom(segundoNombre);
            cliente.setPrimerApe(primerApellido);
            cliente.setSegundoApe(segundoApellido);
            cliente.setNumContacto(numContacto);

            clienteLogica.update(cliente);
            limpiarCampos();

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensaje", ex.getMessage()));
        }
    }

    public void eliminarCliente() {
        try {
            clienteLogica.delete(selectedCliente);
            limpiarCampos();

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Cliente eliminado"));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensaje", ex.getMessage()));
        }
    }

}
