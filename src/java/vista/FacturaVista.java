/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import logica.ClienteLogicaLocal;
import logica.FacturaLogicaLocal;
import modelo.Cliente;
import modelo.Factura;
import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author johan
 */
@ManagedBean
@RequestScoped
public class FacturaVista {

    @EJB
    private FacturaLogicaLocal facturaLogica;
    @EJB
    private ClienteLogicaLocal clienteLogica;

    private SelectOneMenu cmbClientes;
    private Calendar txtFechaFactura;

    private Factura selectedFactura;

    private List<SelectItem> listaClientes;
    private List<Factura> listaFacturas;

    /**
     * Creates a new instance of FacturaVista
     */
    public FacturaVista() {
    }

    public SelectOneMenu getCmbClientes() {
        return cmbClientes;
    }

    public void setCmbClientes(SelectOneMenu cmbClientes) {
        this.cmbClientes = cmbClientes;
    }

    public Calendar getTxtFechaFactura() {
        return txtFechaFactura;
    }

    public void setTxtFechaFactura(Calendar txtFechaFactura) {
        this.txtFechaFactura = txtFechaFactura;
    }

    public Factura getSelectedFactura() {
        return selectedFactura;
    }

    public void setSelectedFactura(Factura selectedFactura) {
        this.selectedFactura = selectedFactura;
    }

    public FacturaLogicaLocal getFacturaLogica() {
        return facturaLogica;
    }

    public void setFacturaLogica(FacturaLogicaLocal facturaLogica) {
        this.facturaLogica = facturaLogica;
    }

    public List<Factura> getListaFacturas() {
        listaFacturas = facturaLogica.listAll();
        return listaFacturas;
    }

    public void setListaFacturas(List<Factura> listaFacturas) {
        this.listaFacturas = listaFacturas;
    }

    public List<SelectItem> getListaClientes() {
        listaClientes = new ArrayList<SelectItem>();
        List<Cliente> clientes = clienteLogica.listAll();

        for (int i = 0; i < clientes.size(); i++) {
            SelectItem cargoItem = new SelectItem(
                    clientes.get(i).getIdCliente(),
                    clientes.get(i).getPrimerNom() + " " + clientes.get(i).getPrimerApe());
            listaClientes.add(cargoItem);
        }
        return listaClientes;
    }

    public void setListaClientes(List<SelectItem> listaClientes) {
        this.listaClientes = listaClientes;
    }

    //-------------- Funciones ---------------
    public void limpiarCampos() {
        cmbClientes.setValue("");
        txtFechaFactura.setValue(null);
    }

    public void seleccionarFactura(SelectEvent e) {
        selectedFactura = (Factura) e.getObject();
        cmbClientes.setValue(selectedFactura.getIdCliente().getIdCliente());
        txtFechaFactura.setValue(selectedFactura.getFechaFactura());
    }

    public void nuevaFactura() {
        try {
            String cliente = cmbClientes.getValue().toString();
            Object fechaFact = txtFechaFactura.getValue();

            DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            String fechaFactura = df.format(fechaFact);

            Factura nuevaFactura = new Factura();
            nuevaFactura.setIdCliente(clienteLogica.find(Integer.parseInt(cliente)));
            nuevaFactura.setFechaFactura(new Date(fechaFactura));

            facturaLogica.store(nuevaFactura);
            limpiarCampos();

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensaje", ex.getMessage()));
        }
    }

    public void actualizarFactura() {
        try {
            String cliente = cmbClientes.getValue().toString();
            Object fechaFact = txtFechaFactura.getValue();

            DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            String fechaFactura = df.format(fechaFact);

            Factura factura = selectedFactura;
            factura.setIdCliente(clienteLogica.find(Integer.parseInt(cliente)));
            factura.setFechaFactura(new Date(fechaFactura));

            facturaLogica.update(factura);
            limpiarCampos();

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensaje", ex.getMessage()));
        }
    }

    public void eliminarFactura() {
        try {
            facturaLogica.delete(selectedFactura);
            limpiarCampos();

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensaje", ex.getMessage()));
        }
    }

}
