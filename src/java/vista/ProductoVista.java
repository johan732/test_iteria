/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import logica.ProductoLogicaLocal;
import modelo.Producto;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author johan
 */
@ManagedBean
@RequestScoped
public class ProductoVista {

    @EJB
    private ProductoLogicaLocal productoLogica;

    private List<Producto> listaProductos;

    private InputText txtNombreProducto;
    private InputText txtDescripcion;
    private InputText txtValor;

    private Producto selectedProducto;

    /**
     * Creates a new instance of ProductoVista
     */
    public ProductoVista() {
    }

    public ProductoLogicaLocal getProductoLogica() {
        return productoLogica;
    }

    public void setProductoLogica(ProductoLogicaLocal productoLogica) {
        this.productoLogica = productoLogica;
    }

    public List<Producto> getListaProductos() {
        listaProductos = productoLogica.listAll();
        return listaProductos;
    }

    public void setListaProductos(List<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }

    public InputText getTxtNombreProducto() {
        return txtNombreProducto;
    }

    public void setTxtNombreProducto(InputText txtNombreProducto) {
        this.txtNombreProducto = txtNombreProducto;
    }

    public InputText getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(InputText txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public InputText getTxtValor() {
        return txtValor;
    }

    public void setTxtValor(InputText txtValor) {
        this.txtValor = txtValor;
    }

    public Producto getSelectedProducto() {
        return selectedProducto;
    }

    public void setSelectedProducto(Producto selectedProducto) {
        this.selectedProducto = selectedProducto;
    }

    //-------------- Funciones ---------------
    public void limpiarCampos() {
        txtNombreProducto.setValue("");
        txtDescripcion.setValue("");
        txtValor.setValue("");
    }

    public void seleccionarProducto(SelectEvent e) {
        selectedProducto = (Producto) e.getObject();
        txtNombreProducto.setValue(selectedProducto.getNombrePro());
        txtDescripcion.setValue(selectedProducto.getDescripProducto());
        txtValor.setValue(selectedProducto.getValorPro());
    }

    public void nuevoProducto() {
        try {
            String nombreProducto = txtNombreProducto.getValue().toString();
            String descripcion = txtDescripcion.getValue().toString();
            String valor = txtValor.getValue().toString();

            Producto nuevoProducto = new Producto();
            nuevoProducto.setNombrePro(nombreProducto);
            nuevoProducto.setDescripProducto(descripcion);
            nuevoProducto.setValorPro(valor);

            productoLogica.store(nuevoProducto);
            limpiarCampos();

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensaje", ex.getMessage()));
        }
    }

    public void actualizarProducto() {
        try {
            String nombreProducto = txtNombreProducto.getValue().toString();
            String descripcion = txtDescripcion.getValue().toString();
            String valor = txtValor.getValue().toString();

            Producto producto = selectedProducto;
            producto.setNombrePro(nombreProducto);
            producto.setDescripProducto(descripcion);
            producto.setValorPro(valor);

            productoLogica.update(producto);
            limpiarCampos();

        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensaje", ex.getMessage()));
        }
    }

    public void eliminarProducto() {
        try {
            productoLogica.delete(selectedProducto);
            limpiarCampos();

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", "Producto eliminado"));
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Mensaje", ex.getMessage()));
        }
    }
}
