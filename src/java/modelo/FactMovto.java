/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author johan
 */
@Entity
@Table(name = "fact_movto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FactMovto.findAll", query = "SELECT f FROM FactMovto f")
    , @NamedQuery(name = "FactMovto.findByIdMovto", query = "SELECT f FROM FactMovto f WHERE f.idMovto = :idMovto")
    , @NamedQuery(name = "FactMovto.findByCantidad", query = "SELECT f FROM FactMovto f WHERE f.cantidad = :cantidad")})
public class FactMovto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_movto")
    private Integer idMovto;
    @Column(name = "cantidad")
    private Integer cantidad;
    @JoinColumn(name = "id_factura", referencedColumnName = "id_factura")
    @ManyToOne(optional = false)
    private Factura idFactura;
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")
    @ManyToOne(optional = false)
    private Producto idProducto;

    public FactMovto() {
    }

    public FactMovto(Integer idMovto) {
        this.idMovto = idMovto;
    }

    public Integer getIdMovto() {
        return idMovto;
    }

    public void setIdMovto(Integer idMovto) {
        this.idMovto = idMovto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Factura getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(Factura idFactura) {
        this.idFactura = idFactura;
    }

    public Producto getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMovto != null ? idMovto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FactMovto)) {
            return false;
        }
        FactMovto other = (FactMovto) object;
        if ((this.idMovto == null && other.idMovto != null) || (this.idMovto != null && !this.idMovto.equals(other.idMovto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.FactMovto[ idMovto=" + idMovto + " ]";
    }
    
}
