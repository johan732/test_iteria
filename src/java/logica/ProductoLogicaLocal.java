/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.List;
import javax.ejb.Local;
import modelo.Producto;

/**
 *
 * @author johan
 */
@Local
public interface ProductoLogicaLocal {

    public List<Producto> listAll();

    public void store(Producto p) throws Exception;

    public void update(Producto p) throws Exception;

    public void delete(Producto p) throws Exception;
}
