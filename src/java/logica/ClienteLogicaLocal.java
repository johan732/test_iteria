/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.List;
import javax.ejb.Local;
import modelo.Cliente;

/**
 *
 * @author johan
 */
@Local
public interface ClienteLogicaLocal {

    public List<Cliente> listAll();

    public void store(Cliente c) throws Exception;

    public void update(Cliente c) throws Exception;

    public void delete(Cliente c) throws Exception;

    public Cliente find(Integer id) throws Exception;

}
