/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.List;
import javax.ejb.Local;
import modelo.Factura;

/**
 *
 * @author johan
 */
@Local
public interface FacturaLogicaLocal {

    public List<Factura> listAll();

    public void store(Factura f) throws Exception;

    public void update(Factura f) throws Exception;

    public void delete(Factura f) throws Exception;

}
