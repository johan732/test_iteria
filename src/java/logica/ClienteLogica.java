/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import modelo.Cliente;
import persistencia.ClienteFacadeLocal;

/**
 *
 * @author johan
 */
@Stateless
public class ClienteLogica implements ClienteLogicaLocal {

    @EJB
    private ClienteFacadeLocal clientesDAO;

    @Override
    public List<Cliente> listAll() {
        return clientesDAO.findAll();
    }

    @Override
    public void store(Cliente c) throws Exception {
        if (c == null) {
            throw new Exception("El cliente no tiene información");
        }

        if (c.getPrimerNom().equals("")) {
            throw new Exception("El primer nombre es requerido");
        }

        if (c.getPrimerApe().equals("")) {
            throw new Exception("El primer apellido es requerido");
        }

        if (c.getNumContacto().equals("")) {
            throw new Exception("El número de contacto es requerido");
        }

        clientesDAO.create(c);
    }

    @Override
    public void update(Cliente c) throws Exception {
        if (c == null) {
            throw new Exception("El cliente no tiene información");
        }

        if (c.getPrimerNom().equals("")) {
            throw new Exception("El primer nombre es requerido");
        }

        if (c.getPrimerApe().equals("")) {
            throw new Exception("El primer apellido es requerido");
        }

        if (c.getNumContacto().equals("")) {
            throw new Exception("El número de contacto es requerido");
        }

        clientesDAO.edit(c);
    }

    @Override
    public void delete(Cliente c) throws Exception {
        Cliente cliente = clientesDAO.find(c.getIdCliente());

        if (cliente == null) {
            throw new Exception("El cliente no existe");
        }
        clientesDAO.remove(cliente);
    }

    @Override
    public Cliente find(Integer id) throws Exception {
        return clientesDAO.find(id);
    }

}
