/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import modelo.Factura;
import persistencia.FacturaFacadeLocal;

/**
 *
 * @author johan
 */
@Stateless
public class FacturaLogica implements FacturaLogicaLocal {

    @EJB
    private FacturaFacadeLocal facturaDAO;

    @Override
    public List<Factura> listAll() {
        return facturaDAO.findAll();
    }

    @Override
    public void store(Factura f) throws Exception {
        if (f == null) {
            throw new Exception("La factura no tiene información");
        }

        if (f.getIdCliente().getIdCliente().equals("")) {
            throw new Exception("El cliente es requerido");
        }

        if (f.getFechaFactura().equals("")) {
            throw new Exception("La factura es requerida");
        }

        facturaDAO.create(f);
    }

    @Override
    public void update(Factura f) throws Exception {
        if (f == null) {
            throw new Exception("La factura no tiene información");
        }

        if (f.getIdCliente().getIdCliente().equals("")) {
            throw new Exception("El cliente es requerido");
        }

        if (f.getFechaFactura().equals("")) {
            throw new Exception("La factura es requerida");
        }

        facturaDAO.edit(f);
    }

    @Override
    public void delete(Factura f) throws Exception {
        Factura factura = facturaDAO.find(f.getIdFactura());

        if (factura == null) {
            throw new Exception("La factura no existe");
        }
        facturaDAO.remove(factura);
    }

}
