/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import modelo.Producto;
import persistencia.ProductoFacadeLocal;

/**
 *
 * @author johan
 */
@Stateless
public class ProductoLogica implements ProductoLogicaLocal {

    @EJB
    private ProductoFacadeLocal productosDAO;

    @Override
    public List<Producto> listAll() {
        return productosDAO.findAll();
    }

    @Override
    public void store(Producto p) throws Exception {
        if (p == null) {
            throw new Exception("El producto no tiene información");
        }

        if (p.getNombrePro().equals("")) {
            throw new Exception("El nombre del producto es requerido");
        }

        if (p.getValorPro().equals("")) {
            throw new Exception("El valor del producto es requerido");
        }
        productosDAO.create(p);
    }

    @Override
    public void update(Producto p) throws Exception {
        if (p == null) {
            throw new Exception("El producto no tiene información");
        }

        if (p.getNombrePro().equals("")) {
            throw new Exception("El nombre del producto es requerido");
        }

        if (p.getValorPro().equals("")) {
            throw new Exception("El valor del producto es requerido");
        }
        productosDAO.edit(p);
    }

    @Override
    public void delete(Producto p) throws Exception {
        Producto producto = productosDAO.find(p.getIdProducto());

        if (producto == null) {
            throw new Exception("El producto no existe");
        }
        productosDAO.remove(producto);

    }

}
