/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.util.List;
import javax.ejb.Local;
import modelo.FactMovto;

/**
 *
 * @author johan
 */
@Local
public interface FactMovtoFacadeLocal {

    void create(FactMovto factMovto);

    void edit(FactMovto factMovto);

    void remove(FactMovto factMovto);

    FactMovto find(Object id);

    List<FactMovto> findAll();

    List<FactMovto> findRange(int[] range);

    int count();
    
}
